(function ($) {
  $(document).ready(function() {  
    // Close
    $("#block-admin-message-admin-message a.admin-message-close").click(function(event) {
      var href = $(this).attr("href");
      $.get(href);
      $(this).parent().slideUp('fast');
      event.preventDefault();
    });
  });
})(jQuery)