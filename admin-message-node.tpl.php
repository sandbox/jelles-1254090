<div class="admin-message sticky">

  <?php // Changing the class on the link will make the javascript no longer function. ?>
  <?php
  if (user_access('close admin messages')) {
    print l(
      'x',
      'admin_message/close/'. $node->nid,
      array(
        'attributes' => array(
          'class' => array(
            'admin-message-close',
          ),
          'title' => t('Close this message')
        )
      )
    );
  }
  ?>

  <?php print drupal_render(node_view($node)); ?>

</div>